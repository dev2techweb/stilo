<?php
/*
* Template Name: Pagina
*/
get_header(); ?>

<div class="container-fluid" style="padding-right:7%; padding-left:7%;">
<h1 class="title t-pink t-center title__page"><?php the_title(); ?></h1>
  <div class="row contenido">
    <div class="col-lg-12">
      <!-- <div class="formulario_envio"> -->
        <?php if (have_posts()): while (have_posts()) : the_post(); ?>
          <?php the_content(); ?>
        <?php endwhile; endif; ?>
      <!-- </div> -->
    </div>
  </div>
</div>
<style>
  .contenido{
    min-height: 20rem;
  }
</style>
<?php get_footer()?>
